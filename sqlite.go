package mksql

import (
	"bytes"
	"fmt"
	"strconv"
)

type sqliteBuilder struct{}

type sqliteContext struct {
	sqlContext
}

func (b *sqliteBuilder) Query(q *Query) (string, []interface{}, error) {
	err := validateQuery(q)
	if err != nil {
		return "", nil, err
	}
	ctx := b.context()
	err = ctx.Query(q)
	if err != nil {
		return "", nil, err
	}
	return ctx.Result.String(), ctx.Parameters, nil
}

func (b *sqliteBuilder) Delete(t *Table, x Expression) (string, []interface{}, error) {
	ctx := b.context()
	ctx.Delete(t, x)
	return ctx.Result.String(), ctx.Parameters, nil
}

func (b *sqliteBuilder) Insert(i *Insert) (string, []interface{}, error) {
	ctx := b.context()
	ctx.Insert(i)
	return ctx.Result.String(), ctx.Parameters, nil
}

func (b *sqliteBuilder) Update(u *Update) (string, []interface{}, error) {
	ctx := b.context()
	ctx.Update(u)
	return ctx.Result.String(), ctx.Parameters, nil
}

func (b *sqliteBuilder) context() *sqliteContext {
	return &sqliteContext{
		sqlContext{
			Parameters: []interface{}{},
			Result:     bytes.NewBuffer(nil),
			tables:     newTables(mySQL),
		},
	}
}

func (pg *sqliteContext) Query(sel *Query) error {
	selSt := pg.withBuffer(func() {
		pg.wr("SELECT ")

		for i, xp := range sel.Select {
			if i != 0 {
				pg.wr(", ")
			}
			pg.Expression(xp)
		}
	})

	whereSt := pg.withBuffer(func() {
		pg.where(sel.Where)
	})

	groupSt := pg.withBuffer(func() {
		if sel.GroupBy != nil {
			pg.wr(" GROUP BY ")
			for i, xp := range sel.GroupBy {
				if i != 0 {
					pg.wr(", ")
				}
				pg.Expression(xp)
			}
		}
	})

	pg.tables.cte(pg.Result)
	pg.Result.Write(selSt)
	pg.wr(" FROM ")
	pg.tables.query(pg.Result)
	pg.Result.Write(whereSt)

	if len(sel.Order) > 0 {
		pg.wr(" ORDER BY ")
		for i, ord := range sel.Order {
			if i > 0 {
				pg.wr(", ")
			}
			pg.Expression(ord.Field)
			if ord.Desc {
				pg.wr(" DESC")
			}
		}
	}

	pg.wr(string(groupSt))

	switch {
	case sel.Limit > 0 && sel.Offset > 0:
		pg.wr(" LIMIT ")
		pg.wr(strconv.Itoa(sel.Limit))
		pg.wr(" OFFSET ")
		pg.wr(strconv.Itoa(sel.Offset))
	case sel.Limit > 0:
		pg.wr(" LIMIT ")
		pg.wr(strconv.Itoa(sel.Limit))
	case sel.Offset > 0:
		pg.wr(" LIMIT -1 OFFSET ")
		pg.wr(strconv.Itoa(sel.Offset))
	}
	return nil
}

func (pg *sqliteContext) Update(update *Update) {
	pg.tables.use(update.Table)
	pg.wr("UPDATE ")
	pg.tables.query(pg.Result)
	pg.wr(" SET ")

	for i, field := range update.Fields {
		if i > 0 {
			pg.wr(", ")
		}
		pg.wr(field.name)
		pg.wr("=?")
		pg.Parameters = append(pg.Parameters, update.Values[i])
	}
	if update.Where != nil {
		pg.where(update.Where)
	}
}

func (pg *sqliteContext) Insert(insert *Insert) {
	pg.wr("INSERT INTO ")
	pg.wr(insert.Table.Name)

	if len(insert.Fields) > 0 {
		pg.wr(" (")
		for i, field := range insert.Fields {
			if i > 0 {
				pg.wr(", ")
			}
			pg.wr(field)
		}
		pg.wr(") VALUES")

		for k, ent := range insert.Values {
			if k > 0 {
				pg.wr(", ")
			}
			pg.wr("(")
			for j, v := range ent {
				if j > 0 {
					pg.wr(", ")
				}
				pg.Parameters = append(pg.Parameters, v)
				pg.wr("?")
			}
			pg.wr(")")
		}
	} else {
		pg.wr(" DEFAULT VALUES")
	}
}

func (pg *sqliteContext) Delete(t *Table, xp Expression) {
	pg.tables.use(t)
	pg.wr("DELETE FROM ")
	pg.tables.query(pg.Result)
	pg.where(xp)
}

func (pg *sqliteContext) Expression(xp Expression) {
	switch x := xp.(type) {
	case *boolExpression:
		xps := x.Expressions()
		pg.pushBrackets(xps)
		for i, p := range xps {
			if i > 0 {
				pg.wr(" ")
				pg.wr(x.op)
				pg.wr(" ")
			}
			pg.Expression(p)
		}
		pg.popBrackets(xps)

	case *compare:
		pg.Expression(x.left)
		pg.wr(" ")
		pg.wrCmp(x.op)
		pg.wr(" ")
		pg.Expression(x.right)

	case *Field:
		name := pg.tables.use(x.table)
		pg.wr(name)
		pg.wr(".")
		pg.wr(x.name)

	case *value:
		pg.Parameters = append(pg.Parameters, x.value)
		pg.wr("?")

	case *count:
		pg.tables.use(x.table)
		pg.wr("COUNT(*)")

	case *aggregate:
		pg.wrAggregate(x)
		pg.wr("(")
		pg.Expression(x.field)
		pg.wr(")")

	case *inExpression:
		if len(x.value) == 1 {
			pg.Expression(x.value[0])
			pg.wr(" IN (")
			for i, v := range x.matches {
				if i > 0 {
					pg.wr(", ")
				}
				pg.Expression(v[0])
			}
			pg.wr(")")
		} else {
			pg.wr("(")
			for i, v := range x.value {
				if i > 0 {
					pg.wr(", ")
				}
				pg.Expression(v)
			}
			pg.wr(") IN (")

			for i, v := range x.matches {
				if i > 0 {
					pg.wr(", ")
				}
				pg.wr("(")
				for i, x := range v {
					if i > 0 {
						pg.wr(", ")
					}
					pg.Expression(x)
				}
				pg.wr(")")
			}

			pg.wr(")")
		}

	default:
		panic(fmt.Errorf("unknown node %T", xp))
	}
}

func (c *sqliteContext) where(x Expression) {
	if x != nil && !x.isEmpty() {
		c.wr(" WHERE ")
		c.Expression(x)
	}
}
