package mksql

import "fmt"

func validateQuery(q *Query) error {
	if q.Offset < 0 {
		return fmt.Errorf("offset must be 0 or greater")
	}
	if q.Limit < 0 {
		return fmt.Errorf("limit must be 0 or greater")
	}
	if q.Offset != 0 || q.Limit != 0 {
		if len(q.Order) == 0 {
			return fmt.Errorf("set order when using limit or offset")
		}
	}
	return nil
}
