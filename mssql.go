package mksql

import (
	"bytes"
	"fmt"
	"strconv"
)

type msBuilder struct{}

type msContext struct {
	sqlContext
	shortField bool
}

func (b *msBuilder) Query(q *Query) (string, []interface{}, error) {
	err := validateQuery(q)
	if err != nil {
		return "", nil, err
	}
	ctx := b.context()
	err = ctx.Query(q)
	if err != nil {
		return "", nil, err
	}
	return ctx.Result.String(), ctx.Parameters, nil
}

func (b *msBuilder) Delete(t *Table, x Expression) (string, []interface{}, error) {
	ctx := b.context()
	ctx.Delete(t, x)
	return ctx.Result.String(), ctx.Parameters, nil
}

func (b *msBuilder) Insert(i *Insert) (string, []interface{}, error) {
	ctx := b.context()
	ctx.Insert(i)
	return ctx.Result.String(), ctx.Parameters, nil
}

func (b *msBuilder) Update(u *Update) (string, []interface{}, error) {
	ctx := b.context()
	ctx.Update(u)
	return ctx.Result.String(), ctx.Parameters, nil
}

func (b *msBuilder) context() *msContext {
	return &msContext{
		sqlContext: sqlContext{
			Parameters: []interface{}{},
			Result:     bytes.NewBuffer(nil),
			tables:     newTables(msSQL),
		},
	}
}

func (pg *msContext) Query(sel *Query) error {
	selSt := pg.withBuffer(func() {
		pg.wr("SELECT ")

		for i, xp := range sel.Select {
			if i != 0 {
				pg.wr(", ")
			}
			pg.Expression(xp)
		}
	})

	whereSt := pg.withBuffer(func() {
		pg.where(sel.Where)
	})

	groupSt := pg.withBuffer(func() {
		if sel.GroupBy != nil {
			pg.wr(" GROUP BY ")
			for i, xp := range sel.GroupBy {
				if i != 0 {
					pg.wr(", ")
				}
				pg.Expression(xp)
			}
		}
	})

	pg.tables.cte(pg.Result)
	pg.Result.Write(selSt)
	pg.wr(" FROM ")
	pg.tables.query(pg.Result)
	pg.Result.Write(whereSt)

	pg.wr(string(groupSt))

	if len(sel.Order) > 0 {
		pg.wr(" ORDER BY ")
		for i, ord := range sel.Order {
			if i > 0 {
				pg.wr(", ")
			}
			pg.Expression(ord.Field)
			if ord.Desc {
				pg.wr(" DESC")
			}
		}
	}

	if sel.Offset != 0 || sel.Limit != 0 {
		pg.wr(" OFFSET ")
		pg.wr(strconv.Itoa(sel.Offset))
		pg.wr(" ROWS")

		if sel.Limit != 0 {
			pg.wr(" FETCH NEXT ")
			pg.wr(strconv.Itoa(sel.Limit))
			pg.wr(" ROWS ONLY")
		}
	}
	return nil
}

func (pg *msContext) Update(update *Update) {
	pg.wr("UPDATE ")
	pg.wr(update.Table.Name)
	pg.wr(" SET ")

	for i, field := range update.Fields {
		if i > 0 {
			pg.wr(", ")
		}
		pg.wr(field.name)
		pg.Parameters = append(pg.Parameters, update.Values[i])
		pg.wr("=$")
		pg.wr(strconv.Itoa(len(pg.Parameters)))
	}
	if update.Where != nil {
		pg.shortField = true
		pg.where(update.Where)
		pg.shortField = false
	}
}

func (pg *msContext) Insert(insert *Insert) {
	pg.wr("INSERT INTO ")
	pg.wr(insert.Table.Name)

	if len(insert.Fields) > 0 {
		pg.wr(" (")
		for i, field := range insert.Fields {
			if i > 0 {
				pg.wr(", ")
			}
			pg.wr(field)
		}
		pg.wr(")")
	}

	if len(insert.Returning) > 0 {
		pg.wr(" OUTPUT ")
		for j, r := range insert.Returning {
			if j > 0 {
				pg.wr(", ")
			}
			pg.wr("Inserted.")
			pg.wr(r)
		}
	}

	if len(insert.Fields) > 0 {
		pg.wr(" VALUES")
		for k, ent := range insert.Values {
			if k > 0 {
				pg.wr(", ")
			}
			pg.wr("(")
			for j, v := range ent {
				if j > 0 {
					pg.wr(", ")
				}
				pg.Parameters = append(pg.Parameters, v)
				pg.wr("$")
				pg.wr(strconv.Itoa(len(pg.Parameters)))
			}
			pg.wr(")")
		}
	} else {
		pg.wr(" DEFAULT VALUES")
	}
}

func (pg *msContext) Delete(t *Table, xp Expression) {
	pg.shortField = true
	whereSt := pg.withBuffer(func() {
		pg.where(xp)
	})

	pg.wr("DELETE FROM ")
	pg.wr(t.Name)
	pg.Result.Write(whereSt)
	pg.shortField = false
}

func (pg *msContext) Expression(xp Expression) {
	switch x := xp.(type) {
	case *boolExpression:
		xps := x.Expressions()
		pg.pushBrackets(xps)
		for i, p := range xps {
			if i > 0 {
				pg.wr(" ")
				pg.wr(x.op)
				pg.wr(" ")
			}
			pg.Expression(p)
		}
		pg.popBrackets(xps)

	case *compare:
		pg.Expression(x.left)
		pg.wr(" ")
		pg.wrCmp(x.op)
		pg.wr(" ")
		pg.Expression(x.right)

	case *Field:
		name := pg.tables.use(x.table)
		if !pg.shortField {
			pg.wr(name)
			pg.wr(".")
		}
		pg.wr(x.name)

	case *value:
		pg.Parameters = append(pg.Parameters, x.value)
		pg.wr("$")
		pg.wr(strconv.Itoa(len(pg.Parameters)))

	case *count:
		pg.tables.use(x.table)
		pg.wr("COUNT(*)")

	case *aggregate:
		pg.wrAggregate(x)
		pg.wr("(")
		pg.Expression(x.field)
		pg.wr(")")

	case *inExpression:
		if len(x.value) == 1 {
			pg.Expression(x.value[0])
			pg.wr(" IN (")
			for i, v := range x.matches {
				if i > 0 {
					pg.wr(", ")
				}
				pg.Expression(v[0])
			}
			pg.wr(")")
		} else {
			pg.wr("(")
			for i, match := range x.matches {
				if i > 0 {
					pg.wr(" OR ")
				}
				pg.wr("(")
				for i, v := range x.value {
					if i > 0 {
						pg.wr(" AND ")
					}
					pg.Expression(v)
					pg.wr(" = ")
					pg.Expression(match[i])
				}

				pg.wr(")")
			}

			pg.wr(")")
		}

	default:
		panic(fmt.Errorf("unknown node %T", xp))
	}
}

func (c *msContext) where(x Expression) {
	if x != nil && !x.isEmpty() {
		c.wr(" WHERE ")
		c.Expression(x)
	}
}
