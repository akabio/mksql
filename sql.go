package mksql

import (
	"bytes"
	"fmt"
	"strconv"
)

type sqlContext struct {
	Parameters []interface{}
	Result     *bytes.Buffer
	tables     tables
	boolDepth  int
}

type tables struct {
	tableMap map[*Table]string
	tables   []*Table
	as       map[string]bool
	dialect  db
}

func newTables(dialect db) tables {
	return tables{
		tableMap: map[*Table]string{},
		as:       map[string]bool{},
		dialect:  dialect,
	}
}

func (sql *sqlContext) wr(s string) {
	sql.Result.WriteString(s)
}

func (sql *sqlContext) withBuffer(f func()) []byte {
	current := sql.Result
	n := bytes.NewBuffer(nil)
	sql.Result = n

	f()

	sql.Result = current
	return n.Bytes()
}

func (sql *sqlContext) pushBrackets(xps []Expression) {
	if len(xps) > 1 && sql.boolDepth > 0 {
		sql.wr("(")
	}
	sql.boolDepth++
}

func (sql *sqlContext) popBrackets(xps []Expression) {
	sql.boolDepth--
	if len(xps) > 1 && sql.boolDepth > 0 {
		sql.wr(")")
	}
}

func (sql *sqlContext) wrAggregate(agg *aggregate) {
	switch agg.f {
	case avg:
		sql.wr("AVG")
	case min:
		sql.wr("MIN")
	case max:
		sql.wr("MAX")
	case sum:
		sql.wr("SUM")
	default:
		panic(fmt.Sprintf("unknown aggregate func %v", agg.f))
	}
}

func (sql *sqlContext) wrCmp(cmp cmpOp) {
	switch cmp {
	case cmpEq:
		sql.wr("=")
	case cmpGt:
		sql.wr(">")
	case cmpGtEq:
		sql.wr(">=")
	case cmpLt:
		sql.wr("<")
	case cmpLtEq:
		sql.wr("<=")
	case cmpNotEq:
		sql.wr("<>")
	case cmpLike:
		sql.wr("LIKE")
	default:
		panic(fmt.Sprintf("unknown compare operator %v", cmp))
	}
}

func (t *tables) use(tab *Table) string {
	k, exists := t.tableMap[tab]
	if !exists {
		for l := 1; true; l++ {
			if l <= len(tab.Name) {
				k = tab.Name[:l]
			} else {
				k = tab.Name + strconv.Itoa(l-len(tab.Name))
			}
			if !t.as[k] && !reserved[t.dialect][k] {
				t.as[k] = true
				break
			}
		}
		t.tableMap[tab] = k
		t.tables = append(t.tables, tab)
	}

	return k
}

func (t *tables) query(buf *bytes.Buffer) {
	i := 0
	for _, tab := range t.tables {
		as := t.tableMap[tab]
		if i > 0 {
			buf.WriteString(", ")
		}
		i++
		buf.WriteString(tab.Name)
		buf.WriteString(" AS ")
		buf.WriteString(as)
	}
}

// cte build common table expression if necessary `WITH xxx AS (...), yyy as (...)`
func (t *tables) cte(buf *bytes.Buffer) {
	ctes := []*Table{}
	for _, tab := range t.tables {
		if tab.Query != "" {
			ctes = append(ctes, tab)
		}
	}

	if len(ctes) == 0 {
		return
	}

	buf.WriteString("WITH ")

	for i, tab := range ctes {
		if i > 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(tab.Name)
		buf.WriteString(" AS (")
		buf.WriteString(tab.Query)
		buf.WriteString(")")
	}

	buf.WriteString(" ")
}
