The internal folder contains test infrastructure.

The db is generated using gitlab.com/akabio/stowage but to not add a external dependency
the generated code is commited.

The test folder contains all the tests. They usually check the generated SQL against the expected string and also let the statement run against the db.

The db can be started using provided docker-compose file. The credentials in the docker compose and the tests are hard coded.