entity foo {
  count int
  name  string
}

entity bar {
  name   string
  foo -> foo
}

entity auto {
  
}