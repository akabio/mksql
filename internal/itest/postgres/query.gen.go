package postgres

import (
	"bytes"
	"fmt"
	"strconv"
)

type Insert struct {
	Entity entities
	Fields []string
	Values [][]interface{}
	ID     []string
}

type Update struct {
	Entity entities
	Values map[string]interface{}
	Where  Condition
}

type entity struct {
	entity string
	as     string
}

type entities []entity

func Entity(e string, as string) entities {
	return entities{{
		entity: e,
		as:     as,
	}}
}

func (es entities) query() string {
	sb := bytes.NewBuffer(nil)
	for i, e := range es {
		if i > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(e.entity)
		if e.as != "" {
			sb.WriteString(" AS " + e.as)
		}
	}
	return sb.String()
}

type Limit struct {
	From int
	To   int
}

func (l *Limit) Query() string {
	sb := bytes.NewBuffer(nil)

	sb.WriteString(" OFFSET ")
	sb.WriteString(strconv.Itoa(l.From))
	sb.WriteString(" ROWS")

	if l.To != 0 {
		sb.WriteString(" FETCH NEXT ")
		sb.WriteString(strconv.Itoa(l.To - l.From))
		sb.WriteString(" ROWS ONLY")
	}

	return sb.String()
}

type Order map[string]bool

func (o Order) Query() string {
	sb := bytes.NewBuffer(nil)
	sb.WriteString("ORDER BY ")
	first := true
	for field, asc := range o {
		if !first {
			sb.WriteString(" ,")
		}
		sb.WriteString(field)
		if asc {
			sb.WriteString(" ASC")
		} else {
			sb.WriteString(" DESC")
		}
		first = false
	}
	return sb.String()
}

type Condition interface {
	Query(*Params) string
}

type ConditionList interface {
	Condition
	Append(c ...Condition)
	Len() int
}

type Params struct {
	params []interface{}
}

func (p *Params) Append(v interface{}) int {
	p.params = append(p.params, v)
	return len(p.params)
}

type Field string

type Value struct {
	Value interface{}
}

type conditionList struct {
	conditions []Condition
	operator   string
}

func (l *conditionList) Query(ps *Params) string {
	sqs := []string{}
	for _, sq := range l.conditions {
		qPart := sq.Query(ps)
		// query parts can be empty (fex: empty || inside an &&)
		// so we need to filter it out
		if len(qPart) > 0 {
			sqs = append(sqs, qPart)
		}
	}
	if len(sqs) == 0 {
		return "" // told you, it can be empty
	}
	if len(sqs) == 1 {
		return sqs[0]
	}

	sb := bytes.NewBuffer(nil)
	for i, p := range sqs {
		if i > 0 {
			sb.WriteString(" " + l.operator + " ")
		}
		sb.WriteString("(" + p + ")")
	}
	return sb.String()
}

func (l *conditionList) Append(c ...Condition) {
	l.conditions = append(l.conditions, c...)
}

func (l *conditionList) Len() int {
	return len(l.conditions)
}

type Contains struct {
	Left  Condition
	Right Condition
}

type Compare struct {
	Operator string
	Left     Condition
	Right    Condition
}

func Equal(left, right Condition) *Compare {
	return &Compare{
		Operator: "=",
		Left:     left,
		Right:    right,
	}
}

func And(c ...Condition) ConditionList {
	return &conditionList{
		conditions: append([]Condition{}, c...),
		operator:   "AND",
	}
}

func Or(c ...Condition) ConditionList {
	return &conditionList{
		conditions: append([]Condition{}, c...),
		operator:   "OR",
	}
}

func (i *Insert) Query() (string, []interface{}) {
	sb := bytes.NewBuffer(nil)
	params := []interface{}{}

	sb.WriteString("INSERT INTO ")
	sb.WriteString(i.Entity.query())
	if len(i.Fields) > 0 {
		sb.WriteString(" (")
		for i, k := range i.Fields {
			if i > 0 {
				sb.WriteString(", ")
			}
			sb.WriteString(k)
		}
		sb.WriteString(") VALUES")

		valIndex := 1
		for k, ent := range i.Values {
			if k > 0 {
				sb.WriteString(", ")
			}
			sb.WriteString("(")
			for j, v := range ent {
				params = append(params, v)
				if j > 0 {
					sb.WriteString(", ")
				}
				sb.WriteString("$")
				sb.WriteString(strconv.Itoa(valIndex))
				valIndex++
			}
			sb.WriteString(")")
		}
	} else {
		sb.WriteString(" DEFAULT VALUES")
	}

	if len(i.ID) > 0 {
		sb.WriteString(" RETURNING ")
		for j, r := range i.ID {
			if j > 0 {
				sb.WriteString(", ")
			}
			sb.WriteString(r)
		}
	}
	return sb.String(), params
}

func (u *Update) Query() (string, []interface{}) {
	sb := bytes.NewBuffer(nil)
	ps := &Params{params: []interface{}{}}

	sb.WriteString("UPDATE ")
	sb.WriteString(u.Entity.query())
	sb.WriteString(" SET ")
	first := true
	idx := 1
	for k, v := range u.Values {
		if !first {
			sb.WriteString(", ")
		}
		sb.WriteString(k)
		sb.WriteString(" = $")
		sb.WriteString(strconv.Itoa(idx))
		ps.params = append(ps.params, v)
		first = false
		idx++
	}
	sb.WriteString(" WHERE ")
	sb.WriteString(u.Where.Query(ps))
	return sb.String(), ps.params
}

func (f Field) Query(ps *Params) string {
	return string(f)
}

func (v *Value) Query(ps *Params) string {
	i := ps.Append(v.Value)
	return "$" + strconv.Itoa(i)
}

func (c *Compare) Query(ps *Params) string {
	l := c.Left.Query(ps)
	r := c.Right.Query(ps)
	return fmt.Sprintf("%v %v %v", l, c.Operator, r)
}

func (a *Contains) Query(ps *Params) string {
	l := a.Left.Query(ps)
	r := a.Right.Query(ps)
	return fmt.Sprintf("%v @> %v", l, r)
}
