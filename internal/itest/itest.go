package itest

import (
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/akabio/mksql/internal/itest/mssql"
	"gitlab.com/akabio/mksql/internal/itest/mysql"
	"gitlab.com/akabio/mksql/internal/itest/postgres"
	"gitlab.com/akabio/mksql/internal/itest/sqlite"
)

func MSExec(query string, values []interface{}) error {
	con := `Server=localhost;database=test;Persist Security Info=False;User ID=sa;Password=superPassword42;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;`
	st, err := mssql.New(con)
	if err != nil {
		return err
	}

	err = st.Reset()
	if err != nil {
		return err
	}

	db, err := sql.Open("mssql", con)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = db.Exec(query, values...)
	return err
}

func PGExec(query string, values []interface{}) error {
	con := "host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable"
	st, err := postgres.New(con)
	if err != nil {
		return err
	}

	err = st.Reset()
	if err != nil {
		return err
	}

	db, err := sql.Open("postgres", con)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = db.Exec(query, values...)
	return err
}

func MyExec(query string, values []interface{}) error {
	con := "mysql:mysql@tcp(127.0.0.1)/mysql"
	st, err := mysql.New(con)
	if err != nil {
		return err
	}

	err = st.Reset()
	if err != nil {
		return err
	}

	db, err := sql.Open("mysql", con)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = db.Exec(query, values...)
	return err
}

func SQLiteExec(query string, values []interface{}) error {
	con := "file:memdb1?mode=memory&cache=shared"
	st, err := sqlite.New(con)
	if err != nil {
		return err
	}

	err = st.Reset()
	if err != nil {
		return err
	}

	db, err := sql.Open("sqlite3", con)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = db.Exec(query, values...)
	return err
}
