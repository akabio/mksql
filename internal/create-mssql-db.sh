#/bin/bash

for i in {1..10};
do
    /opt/mssql-tools/bin/sqlcmd -S mssql -U sa -P "$SA_PASSWORD" -q "CREATE DATABASE test"
    if [ $? -eq 0 ]
    then
        echo "setup.sql completed"
        break
    else
        echo "not ready yet..."
        sleep 1
    fi
done