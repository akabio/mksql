package test

import (
	"testing"

	q "gitlab.com/akabio/mksql"
)

var insertTests = []testCase{
	{
		i: &q.Insert{
			Table:     fooTab,
			Fields:    []string{"count", "name"},
			Values:    [][]interface{}{{3, "three"}},
			Returning: []string{"id"},
		},
		pg: "INSERT INTO foo (count, name) VALUES($1, $2) RETURNING id",
		my: "INSERT INTO foo (count, name) VALUES(?, ?)",
		sl: "INSERT INTO foo (count, name) VALUES(?, ?)",
		ms: "INSERT INTO foo (count, name) OUTPUT Inserted.id VALUES($1, $2)",
	},
	{
		i: &q.Insert{
			Table:     autoTab,
			Fields:    []string{},
			Values:    [][]interface{}{},
			Returning: []string{"id"},
		},
		pg: "INSERT INTO auto DEFAULT VALUES RETURNING id",
		my: "INSERT INTO auto () VALUES()",
		sl: "INSERT INTO auto DEFAULT VALUES",
		ms: "INSERT INTO auto OUTPUT Inserted.id DEFAULT VALUES",
	},
}

func TestInserts(t *testing.T) {
	run(t, insertTests)
}
