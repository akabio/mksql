package test

import (
	"testing"

	q "gitlab.com/akabio/mksql"
)

var updateTests = []testCase{
	{
		u: &q.Update{
			Table:  fooTab,
			Fields: fooTab.FieldTypes("count", "name"),
			Values: []interface{}{3, "three"},
			Where:  q.Eq(fooTab.Field("name"), q.Value("old")),
		},
		pg: "UPDATE foo AS f SET count=$1, name=$2 WHERE f.name = $3",
		my: "UPDATE foo AS f SET count=?, name=? WHERE f.name = ?",
		sl: "UPDATE foo AS f SET count=?, name=? WHERE f.name = ?",
		ms: "UPDATE foo SET count=$1, name=$2 WHERE name = $3",
	},
}

func TestUpdates(t *testing.T) {
	run(t, updateTests)
}
