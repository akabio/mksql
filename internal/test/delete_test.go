package test

import (
	"testing"

	"github.com/akabio/expect"
	q "gitlab.com/akabio/mksql"
	"gitlab.com/akabio/mksql/internal/itest"
)

type deleteTest struct {
	x  q.Expression
	pg string
	my string
	ms string
	sl string
}

var deleteTests = []deleteTest{
	{
		x: q.And(
			q.Eq(fooTab.Field("count"), q.Value(10)),
		),
		pg: "DELETE FROM foo AS f WHERE f.count = $1",
		my: "DELETE FROM foo AS f WHERE f.count = ?",
		sl: "DELETE FROM foo AS f WHERE f.count = ?",
		ms: "DELETE FROM foo WHERE count = $1",
	}, {
		x:  q.And(),
		pg: "DELETE FROM foo AS f",
		my: "DELETE FROM foo AS f",
		sl: "DELETE FROM foo AS f",
		ms: "DELETE FROM foo",
	},
}

func TestDeletes(t *testing.T) {
	pg := q.NewPostgres()
	my := q.NewMySQL()
	ms := q.NewMSSQL()
	sl := q.NewSQLite()
	for _, xt := range deleteTests {
		t.Run("", func(t *testing.T) {
			t.Run("postgres", func(t *testing.T) {
				pgs, values, _ := pg.Delete(fooTab, xt.x)
				expect.Value(t, "pg sql", pgs).ToBe(xt.pg)
				expect.Error(t, itest.PGExec(pgs, values)).ToBe(nil)
			})

			t.Run("mysql", func(t *testing.T) {
				mys, values, _ := my.Delete(fooTab, xt.x)
				expect.Value(t, "my sql", mys).ToBe(xt.my)
				expect.Error(t, itest.MyExec(mys, values)).ToBe(nil)
			})

			t.Run("mssql", func(t *testing.T) {
				mss, values, _ := ms.Delete(fooTab, xt.x)
				expect.Value(t, "ms sql", mss).ToBe(xt.ms)
				expect.Error(t, itest.MSExec(mss, values)).ToBe(nil)
			})

			t.Run("sqlite", func(t *testing.T) {
				mss, values, _ := sl.Delete(fooTab, xt.x)
				expect.Value(t, "sqlite", mss).ToBe(xt.sl)
				expect.Error(t, itest.SQLiteExec(mss, values)).ToBe(nil)
			})
		})
	}
}
