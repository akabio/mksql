package test

import (
	"testing"

	"gitlab.com/akabio/mksql"
	q "gitlab.com/akabio/mksql"
)

var (
	fooTab   = &q.Table{Name: "foo"}
	barTab   = &q.Table{Name: "bar"}
	autoTab  = &q.Table{Name: "auto"}
	cteTable = &q.Table{
		Name:  "cte",
		Query: "SELECT count*3 as count, name, 'Dr' AS title FROM foo",
	}
	cte2Table = &q.Table{
		Name:  "cte2",
		Query: "SELECT name, 'Dr' AS title FROM bar",
	}
)

var (
	count    = fooTab.Field("count")
	name     = fooTab.Field("name")
	cteTitle = cteTable.Field("title")
	cteName  = cteTable.Field("name")
	cte2Name = cte2Table.Field("name")
)

var queryTests = []testCase{
	{
		t: "simple select where",
		q: &q.Query{
			Select: fooTab.Fields("count", "name"),
			Where: q.And(
				q.Eq(fooTab.Field("count"), q.Value(10)),
				q.Eq(barTab.Field("name"), q.Value("freddy")),
			),
		},
		pg: "SELECT f.count, f.name FROM foo AS f, bar AS b WHERE f.count = $1 AND b.name = $2",
		my: "SELECT f.count, f.name FROM foo AS f, bar AS b WHERE f.count = ? AND b.name = ?",
		sl: "SELECT f.count, f.name FROM foo AS f, bar AS b WHERE f.count = ? AND b.name = ?",
		ms: "SELECT f.count, f.name FROM foo AS f, bar AS b WHERE f.count = $1 AND b.name = $2",
	},
	{
		t: "select all, no where",
		q: &q.Query{
			Select: fooTab.Fields("count"),
		},
		pg: "SELECT f.count FROM foo AS f",
		my: "SELECT f.count FROM foo AS f",
		sl: "SELECT f.count FROM foo AS f",
		ms: "SELECT f.count FROM foo AS f",
	},
	{
		t: "simple order by",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Order:  []q.Order{{Field: fooTab.Field("name")}},
		},
		pg: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.name",
		my: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.name",
		sl: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.name",
		ms: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.name",
	},
	{
		t: "order by descending",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Order:  []q.Order{{Field: fooTab.Field("name"), Desc: true}},
		},
		pg: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.name DESC",
		my: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.name DESC",
		sl: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.name DESC",
		ms: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.name DESC",
	},
	{
		t: "order by multiple fields",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Order: []q.Order{
				{Field: fooTab.Field("name"), Desc: true},
				{Field: fooTab.Field("count")},
			},
		},
		pg: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.name DESC, f.count",
		my: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.name DESC, f.count",
		sl: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.name DESC, f.count",
		ms: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.name DESC, f.count",
	},
	{
		t: "order by with limit and offset",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Order:  []q.Order{{Field: fooTab.Field("count")}},
			Limit:  10,
			Offset: 5,
		},
		pg: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.count LIMIT 10 OFFSET 5",
		my: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.count LIMIT 5, 10",
		sl: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.count LIMIT 10 OFFSET 5",
		ms: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.count OFFSET 5 ROWS FETCH NEXT 10 ROWS ONLY",
	},
	{
		t: "order by with limit",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Order:  []q.Order{{Field: fooTab.Field("count")}},
			Limit:  10,
		},
		pg: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.count LIMIT 10",
		my: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.count LIMIT 10",
		sl: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.count LIMIT 10",
		ms: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.count OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY",
	},
	{
		t: "order by with offset",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Order:  []q.Order{{Field: fooTab.Field("count")}},
			Offset: 3,
		},
		pg: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.count OFFSET 3",
		my: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.count LIMIT 3, 18446744073709551615",
		sl: "SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.count LIMIT -1 OFFSET 3",
		ms: "SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.count OFFSET 3 ROWS",
	},
	{
		t: "order by with negative offset",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Order:  []q.Order{{Field: fooTab.Field("count")}},
			Offset: -3,
		},
		pge: "offset must be 0 or greater",
		mye: "offset must be 0 or greater",
		sle: "offset must be 0 or greater",
		mse: "offset must be 0 or greater",
	},
	{
		t: "order by with negative limit",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Order:  []q.Order{{Field: fooTab.Field("count")}},
			Limit:  -1,
		},
		pge: "limit must be 0 or greater",
		mye: "limit must be 0 or greater",
		sle: "limit must be 0 or greater",
		mse: "limit must be 0 or greater",
	},
	{
		t: "limit with no order",
		q: &q.Query{
			Select: fooTab.Fields("count"),
			Where:  q.Eq(fooTab.Field("name"), q.Value("panan")),
			Limit:  10,
			Offset: 5,
		},
		pge: "set order when using limit or offset",
		mye: "set order when using limit or offset",
		sle: "set order when using limit or offset",
		mse: "set order when using limit or offset",
	},
	{
		t: "group by field",
		q: &q.Query{
			Select:  []mksql.Expression{mksql.Count(fooTab), fooTab.Field("name")},
			GroupBy: []mksql.Expression{fooTab.Field("name")},
		},
		pg: "SELECT COUNT(*), f.name FROM foo AS f GROUP BY f.name",
		my: "SELECT COUNT(*), f.name FROM foo AS f GROUP BY f.name",
		sl: "SELECT COUNT(*), f.name FROM foo AS f GROUP BY f.name",
		ms: "SELECT COUNT(*), f.name FROM foo AS f GROUP BY f.name",
	},
	{
		t: "group aggregate functions",
		q: &q.Query{
			Select: []mksql.Expression{
				mksql.Count(fooTab),
				mksql.Avg(fooTab.Field("count")),
				mksql.Min(fooTab.Field("count")),
				mksql.Max(fooTab.Field("count")),
				mksql.Sum(fooTab.Field("count")),
			},
			GroupBy: []mksql.Expression{fooTab.Field("name")},
		},
		pg: "SELECT COUNT(*), AVG(f.count), MIN(f.count), MAX(f.count), SUM(f.count) FROM foo AS f GROUP BY f.name",
		my: "SELECT COUNT(*), AVG(f.count), MIN(f.count), MAX(f.count), SUM(f.count) FROM foo AS f GROUP BY f.name",
		sl: "SELECT COUNT(*), AVG(f.count), MIN(f.count), MAX(f.count), SUM(f.count) FROM foo AS f GROUP BY f.name",
		ms: "SELECT COUNT(*), AVG(f.count), MIN(f.count), MAX(f.count), SUM(f.count) FROM foo AS f GROUP BY f.name",
	},
	{
		t: "comparison operators",
		q: &q.Query{
			Select: []mksql.Expression{
				name,
			},
			Where: mksql.And(
				mksql.Eq(count, mksql.Value(3)),
				mksql.Less(count, mksql.Value(3)),
				mksql.Greater(count, mksql.Value(3)),
				mksql.LessEq(count, mksql.Value(3)),
				mksql.GreaterEq(count, mksql.Value(3)),
				mksql.NotEq(count, mksql.Value(3)),
				mksql.Like(name, mksql.Value("foo")),
			),
		},
		pg: "SELECT f.name FROM foo AS f WHERE f.count = $1 AND f.count < $2 AND f.count > $3 AND f.count <= $4 AND f.count >= $5 AND f.count <> $6 AND f.name LIKE $7",
		my: "SELECT f.name FROM foo AS f WHERE f.count = ? AND f.count < ? AND f.count > ? AND f.count <= ? AND f.count >= ? AND f.count <> ? AND f.name LIKE ?",
		sl: "SELECT f.name FROM foo AS f WHERE f.count = ? AND f.count < ? AND f.count > ? AND f.count <= ? AND f.count >= ? AND f.count <> ? AND f.name LIKE ?",
		ms: "SELECT f.name FROM foo AS f WHERE f.count = $1 AND f.count < $2 AND f.count > $3 AND f.count <= $4 AND f.count >= $5 AND f.count <> $6 AND f.name LIKE $7",
	},
	{
		q: &q.Query{
			Select: []mksql.Expression{name},
			Where:  q.In([]q.Expression{fooTab.Field("count")}, []q.Expression{q.Value(1)}, []q.Expression{q.Value(2)}),
		},
		pg: "SELECT f.name FROM foo AS f WHERE f.count IN ($1, $2)",
		my: "SELECT f.name FROM foo AS f WHERE f.count IN (?, ?)",
		sl: "SELECT f.name FROM foo AS f WHERE f.count IN (?, ?)",
		ms: "SELECT f.name FROM foo AS f WHERE f.count IN ($1, $2)",
	},
	{
		q: &q.Query{
			Select: []mksql.Expression{name},
			Where:  q.In([]q.Expression{fooTab.Field("count"), fooTab.Field("name")}, []q.Expression{q.Value(1), q.Value("steve")}, []q.Expression{q.Value(2), q.Value("peter")}),
		},
		pg: "SELECT f.name FROM foo AS f WHERE (f.count, f.name) IN (($1, $2), ($3, $4))",
		my: "SELECT f.name FROM foo AS f WHERE (f.count, f.name) IN ((?, ?), (?, ?))",
		sl: "SELECT f.name FROM foo AS f WHERE (f.count, f.name) IN ((?, ?), (?, ?))",
		ms: "SELECT f.name FROM foo AS f WHERE ((f.count = $1 AND f.name = $2) OR (f.count = $3 AND f.name = $4))",
	},

	{
		q: &q.Query{
			Select: []mksql.Expression{cteTitle, cteName},
			Where:  mksql.Eq(cteTitle, mksql.Value("Dr")),
		},
		pg: "WITH cte AS (SELECT count*3 as count, name, 'Dr' AS title FROM foo) SELECT c.title, c.name FROM cte AS c WHERE c.title = $1",
		my: "WITH cte AS (SELECT count*3 as count, name, 'Dr' AS title FROM foo) SELECT c.title, c.name FROM cte AS c WHERE c.title = ?",
		sl: "WITH cte AS (SELECT count*3 as count, name, 'Dr' AS title FROM foo) SELECT c.title, c.name FROM cte AS c WHERE c.title = ?",
		ms: "WITH cte AS (SELECT count*3 as count, name, 'Dr' AS title FROM foo) SELECT c.title, c.name FROM cte AS c WHERE c.title = $1",
	},

	{
		q: &q.Query{
			Select: []mksql.Expression{cteTitle, cte2Name},
			Where:  mksql.Eq(cteName, cte2Name),
		},
		pg: "WITH cte AS (SELECT count*3 as count, name, 'Dr' AS title FROM foo), cte2 AS (SELECT name, 'Dr' AS title FROM bar) SELECT c.title, ct.name FROM cte AS c, cte2 AS ct WHERE c.name = ct.name",
		my: "WITH cte AS (SELECT count*3 as count, name, 'Dr' AS title FROM foo), cte2 AS (SELECT name, 'Dr' AS title FROM bar) SELECT c.title, ct.name FROM cte AS c, cte2 AS ct WHERE c.name = ct.name",
		sl: "WITH cte AS (SELECT count*3 as count, name, 'Dr' AS title FROM foo), cte2 AS (SELECT name, 'Dr' AS title FROM bar) SELECT c.title, ct.name FROM cte AS c, cte2 AS ct WHERE c.name = ct.name",
		ms: "WITH cte AS (SELECT count*3 as count, name, 'Dr' AS title FROM foo), cte2 AS (SELECT name, 'Dr' AS title FROM bar) SELECT c.title, ct.name FROM cte AS c, cte2 AS ct WHERE c.name = ct.name",
	},
}

func TestQueries(t *testing.T) {
	run(t, queryTests)
}
