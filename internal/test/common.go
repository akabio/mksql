package test

import (
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/akabio/mksql"
	"gitlab.com/akabio/mksql/internal/itest"
)

type testCase struct {
	t string
	// one of the following needs to be set and will be generated
	q *mksql.Query
	i *mksql.Insert
	u *mksql.Update

	// expected generated statement
	pg string
	my string
	ms string
	sl string

	// expected err messages
	pge string
	mye string
	mse string
	sle string
}

func run(t *testing.T, tcs []testCase) {
	pg := mksql.NewPostgres()
	my := mksql.NewMySQL()
	sl := mksql.NewSQLite()
	ms := mksql.NewMSSQL()

	for _, tc := range tcs {
		t.Run(tc.t, func(t *testing.T) {
			t.Run("postgres", func(t *testing.T) {
				runOne(t, tc, pg, tc.pg, tc.pge, itest.PGExec)
			})

			t.Run("mysql", func(t *testing.T) {
				runOne(t, tc, my, tc.my, tc.mye, itest.MyExec)
			})

			t.Run("sqlite", func(t *testing.T) {
				runOne(t, tc, sl, tc.sl, tc.sle, itest.SQLiteExec)
			})

			t.Run("mssql", func(t *testing.T) {
				runOne(t, tc, ms, tc.ms, tc.mse, itest.MSExec)
			})
		})
	}
}

func runOne(t *testing.T, tc testCase, builder mksql.Builder, expStatement string, expectedErr string, exec func(string, []interface{}) error) {
	t.Helper()

	var st string
	var values []interface{}
	var err error
	switch {
	case tc.q != nil:
		st, values, err = builder.Query(tc.q)
	case tc.i != nil:
		st, values, err = builder.Insert(tc.i)
	case tc.u != nil:
		st, values, err = builder.Update(tc.u)
	}

	expect.Error(t, err).Message().ToBe(expectedErr)
	expect.Value(t, "generated sql", st).ToBe(expStatement)

	if expectedErr == "" {
		// test against db, this must always succeed
		err := exec(st, values)
		expect.Error(t, err).Message().ToBe("")
		if err != nil {
			t.Log(expStatement)
		}
	}
}
