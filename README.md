mksql
=====

Generating sql code using code.

### Main use case is:
  - Abstract away some differences between vendors.
  - Be able to generate queries dynamically withouth having to fiddle around with string concatenations.
  - Limit the possibilities to a subset for limiting usage of vendor specific behaviour and keep it simple.

### What it can not do:
  - Only a subset is included so far.
  - No JOIN syntax, you can use implicit joins though by adding a where expression with values from different tables.
  - No DDL statements can be generated, it's intended for runtime.
  - No escaping of database keywords (yet). Escaped keywords can be misleading (SELECT "FROM", "SELECT" FROM "SELECT").
    The problem can be avoided at designtime by avoiding keywords.

### Example:

    table := &mksql.Table{Name: "foo"}
    query := &mksql.Query{
      Select: table.Fields("count"),
      Where:  mksql.Eq(table.Field("name"), mksql.Value("Pan")),
      Order:  mksql.Order{Field: table.Field("count")},
      Limit:  10,
      Offset: 5,
    },

The query can be created using:

    statement, params, err := q.NewPostgres().Query(query)

And Produces a statement, depending on the db:

    postgres: SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.count LIMIT 10 OFFSET 5
    my sql: SELECT f.count FROM foo AS f WHERE f.name = ? ORDER BY f.count LIMIT 5, 10
    ms sql: SELECT f.count FROM foo AS f WHERE f.name = $1 ORDER BY f.count OFFSET 5 ROWS FETCH NEXT 10 ROWS ONLY

