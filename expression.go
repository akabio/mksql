package mksql

type Expression interface {
	// isExpresion is just to force the interface
	isExpression() bool
	isEmpty() bool
}

type Expressions interface {
	Expression
	Append(...Expression)
}

type ExpressionTuples interface {
	Expression
	Append(...[]Expression)
}

type boolExpression struct {
	expressions []Expression
	op          string
}

func (b *boolExpression) Append(x ...Expression) {
	b.expressions = append(b.expressions, x...)
}

func (b *boolExpression) isExpression() bool {
	return true
}

func (b *boolExpression) isEmpty() bool {
	if b == nil {
		return true
	}
	for _, x := range b.expressions {
		if !x.isEmpty() {
			return false
		}
	}
	return true
}

func (b *boolExpression) Expressions() []Expression {
	return skipEmpty(b.expressions)
}

type cmpOp int

const (
	cmpEq cmpOp = iota
	cmpGt
	cmpLt
	cmpGtEq
	cmpLtEq
	cmpNotEq
	cmpLike
)

type compare struct {
	left  Expression
	right Expression
	op    cmpOp
}

func (c *compare) isExpression() bool {
	return true
}

func (c *compare) isEmpty() bool {
	return false
}

type value struct {
	value interface{}
}

func (v *value) isExpression() bool {
	return true
}

func (v *value) isEmpty() bool {
	return false
}

type count struct {
	table *Table
}

func (c *count) isExpression() bool {
	return true
}

func (c *count) isEmpty() bool {
	return false
}

type aggregateFunc int

const (
	avg aggregateFunc = iota
	min
	max
	sum
)

type aggregate struct {
	f     aggregateFunc
	field *Field
}

func (a *aggregate) isExpression() bool {
	return true
}

func (a *aggregate) isEmpty() bool {
	return false
}

func And(xps ...Expression) Expressions {
	return &boolExpression{expressions: xps, op: "AND"}
}

func Or(xps ...Expression) Expressions {
	return &boolExpression{expressions: xps, op: "OR"}
}

func Eq(l, r Expression) Expression {
	return cmp(l, cmpEq, r)
}

func NotEq(l, r Expression) Expression {
	return cmp(l, cmpNotEq, r)
}

func Less(l, r Expression) Expression {
	return cmp(l, cmpLt, r)
}

func LessEq(l, r Expression) Expression {
	return cmp(l, cmpLtEq, r)
}

func Greater(l, r Expression) Expression {
	return cmp(l, cmpGt, r)
}

func GreaterEq(l, r Expression) Expression {
	return cmp(l, cmpGtEq, r)
}

func Like(l, r Expression) Expression {
	return cmp(l, cmpLike, r)
}

type inExpression struct {
	value   []Expression
	matches [][]Expression
}

func (i *inExpression) Append(xs ...[]Expression) {
	i.matches = append(i.matches, xs...)
}

func (i *inExpression) isEmpty() bool {
	// return always false because the empty in expression matches nothing
	// whereas most other expressions match everything
	return false
}

func (i *inExpression) isExpression() bool {
	return true
}

func In(value []Expression, matches ...[]Expression) ExpressionTuples {
	return &inExpression{
		value:   value,
		matches: matches,
	}
}

func cmp(l Expression, op cmpOp, r Expression) Expression {
	return &compare{
		left:  l,
		right: r,
		op:    op,
	}
}

func Value(val interface{}) Expression {
	return &value{value: val}
}

func Count(t *Table) Expression {
	return &count{table: t}
}

func Avg(f *Field) Expression {
	return &aggregate{field: f, f: avg}
}

func Min(f *Field) Expression {
	return &aggregate{field: f, f: min}
}

func Max(f *Field) Expression {
	return &aggregate{field: f, f: max}
}

func Sum(f *Field) Expression {
	return &aggregate{field: f, f: sum}
}

func skipEmpty(xs []Expression) []Expression {
	nxs := []Expression{}
	for _, x := range xs {
		if !x.isEmpty() {
			nxs = append(nxs, x)
		}
	}
	return nxs
}
