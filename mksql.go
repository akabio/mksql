package mksql

type Builder interface {
	Query(q *Query) (string, []interface{}, error)
	Delete(t *Table, x Expression) (string, []interface{}, error)
	Insert(i *Insert) (string, []interface{}, error)
	Update(u *Update) (string, []interface{}, error)
}

func NewMSSQL() Builder {
	return &msBuilder{}
}

func NewMySQL() Builder {
	return &myBuilder{}
}

func NewPostgres() Builder {
	return &pgBuilder{}
}

func NewSQLite() Builder {
	return &sqliteBuilder{}
}
