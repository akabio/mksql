package mksql

import (
	"testing"

	"github.com/akabio/expect"
)

var fooTab = &Table{Name: "foo"}

type expressionTest struct {
	x  Expression
	pg string
}

var expressionTests = []expressionTest{
	{
		x:  Eq(fooTab.Field("lemon"), Value(3)),
		pg: "f.lemon = $1",
	}, {
		x:  And(Or(), Or()),
		pg: "",
	},
}

func TestExpressions(t *testing.T) {
	pg := NewPostgres()
	for _, xt := range expressionTests {
		t.Run("", func(t *testing.T) {
			pgb := pg.(*pgBuilder).context()
			pgb.Expression(xt.x)
			expect.Value(t, "sql", pgb.Result.String()).ToBe(xt.pg)
		})
	}
}
