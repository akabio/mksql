package mksql

import (
	"strings"
	"testing"

	"github.com/akabio/expect"
)

func TestTableAliasCount(t *testing.T) {
	tabs := newTables(postgres)
	tabs.use(&Table{Name: "foo"})
	tabs.use(&Table{Name: "fool"})
	tabs.use(&Table{Name: "foo"})
	tabs.use(&Table{Name: "foo"})
	tabs.use(&Table{Name: "foo"})
	tabs.use(&Table{Name: "fo"})
	tabs.use(&Table{Name: "foo"})
	tabs.use(&Table{Name: "foo"})
	names := []string{}
	for _, t := range tabs.tables {
		names = append(names, tabs.tableMap[t])
	}
	expect.Value(t, "aliases", strings.Join(names, ", ")).ToBe("f, fo, foo, foo1, foo2, fo1, foo3, foo4")
}

func TestTableAliasReservedWords(t *testing.T) {
	tabs := newTables(postgres)
	tabs.use(&Table{Name: "do"})
	tabs.use(&Table{Name: "do"})
	tabs.use(&Table{Name: "do"})
	tabs.use(&Table{Name: "for"})
	tabs.use(&Table{Name: "for"})
	tabs.use(&Table{Name: "for"})
	tabs.use(&Table{Name: "for"})
	tabs.use(&Table{Name: "doc"})
	names := []string{}
	for _, t := range tabs.tables {
		names = append(names, tabs.tableMap[t])
	}
	expect.Value(t, "aliases", strings.Join(names, ", ")).ToBe("d, do1, do2, f, fo, for1, for2, doc")
}
