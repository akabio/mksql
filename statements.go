package mksql

type Query struct {
	Select  []Expression
	Where   Expression
	Order   []Order
	Offset  int
	Limit   int
	GroupBy []Expression
}

type Insert struct {
	Table     *Table
	Fields    []string
	Values    [][]interface{}
	Returning []string
}

type Update struct {
	Table  *Table
	Fields []*Field
	Values []interface{}
	Where  Expression
}

type Order struct {
	Field *Field
	Desc  bool
}

type Table struct {
	Name string
	// if Query is not empty it will be an inline table
	Query string
}

type Field struct {
	table *Table
	name  string
}

func (f *Field) isExpression() bool {
	return true
}

func (f *Field) isEmpty() bool {
	return false
}

func (t *Table) Field(name string) *Field {
	return &Field{
		table: t,
		name:  name,
	}
}

func (t *Table) FieldTypes(names ...string) []*Field {
	exps := make([]*Field, len(names))
	for i, n := range names {
		exps[i] = t.Field(n)
	}
	return exps
}

func (t *Table) Fields(names ...string) []Expression {
	exps := make([]Expression, len(names))
	for i, n := range names {
		exps[i] = t.Field(n)
	}
	return exps
}
