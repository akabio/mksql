module gitlab.com/akabio/mksql

go 1.16

require (
	github.com/akabio/expect v0.10.0
	github.com/denisenkom/go-mssqldb v0.10.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/kr/pretty v0.2.1 // indirect
	github.com/lib/pq v1.10.2
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
)
